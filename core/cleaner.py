"""
%LENNY_FACE%
"""

import json
import re
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import cpu_count

from .search_engine.google import GoogleSearch
from .search_engine.yandex import YaSearch
from .vk_wrapper.api import VKApiWrapper


class CommentsCleaner:
    """
    Class for clearing vk comments
    """

    URLS_FILE = '.urls'
    CACHE_FILE = '.vkcache'
    URL_PATTERN = re.compile("https?:\/\/vk\.com\/(photos|video|wall|topic)(-?\d+)_?(\d+)?.*\??")

    def __init__(self, vk_app_id, name, surname):
        """  """
        self._name = '{} {}'.format(surname, name)
        self._vk_api = VKApiWrapper(vk_app_id)
        self._cache = set()
        self._urls = set()

    def __enter__(self):
        self._load_cache()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._save_cache()

    def remove_all_vk_comments(self):
        """ Remove comments using all possible methods """
        self._by_vk_newsfeed_api()
        self._by_search_engines()
        # self._by_urls_list()

    def _by_vk_newsfeed_api(self):
        """ Remove comments using vk api """
        res = self._vk_api.newsfeed_comments()
        while True:
            if not res['items']:
                break

            min_date = None
            for item in res['items']:
                if not min_date or min_date > item['date']:
                    min_date = item['date']

                if item['source_id'] == self._vk_api.user_id:
                    continue

                self._remove_comments(
                    item['type'],
                    item['source_id'],
                    item['post_id']
                )
            res = self._vk_api.newsfeed_comments(
                end_time=min_date - 1
            )

    def _by_search_engines(self):
        """ Remove comments using search engines """
        with ThreadPoolExecutor(max_workers=cpu_count()) as executor: # 1) as executor: #
            for search_engine in (
                YaSearch,
                GoogleSearch,
            ):
                print("{}. Start searching".format(search_engine.__name__))

                for url in search_engine().search(self._name):
                    executor.submit(self._process_result, url)

                print("{}. No more results".format(search_engine.__name__), end='\n\n')
        self._save_urls()

    def _by_urls_list(self):
        """ Remove comments using local url list """
        self._load_urls()
        with ThreadPoolExecutor(max_workers=cpu_count()) as executor:
            for url in self._urls:
                executor.submit(self._process_result, url)

    def _process_result(self, url):
        """
        Process url

        Args:
            url (str): vk resource url
        """
        print('.', end='')
        url_match = self.URL_PATTERN.match(url)
        if url_match and url_match[3] and url_match[1] not in ('id', 'group', ''):
            self._urls.add(url)
            item_type = url_match[1].replace('wall', 'post').replace('photos', 'photo')
            owner_id = int(url_match[2])
            post_id = int(url_match[3])
            comments = self._remove_comments(
                item_type,
                owner_id,
                post_id
            )
            for _comment in comments:
                print("[{}] Found comment: {}".format(url, _comment))

    def _remove_comments(self, item_type, owner_id, object_id):
        """
        Remove all comments from post

        Args:
            item_type (str): resource type
            owner_id (int): post owner id
            object_id (int): post id
        """
        uid = (item_type, owner_id, object_id)
        if uid in self._cache:
            return []

        try:
            comments = self._vk_api.get_comments(item_type, **{
                'owner_id': owner_id,
                'object_id': object_id,
            })
        except Exception as exc:
            for reason in (
                'was deleted',
                'Access to post comments denied',
                'post was not found',
                'Access denied',
            ):
                if reason in str(exc):
                    self._cache.add(uid)
                    break
            else:
                print("Remove comments failed: {}".format(str(exc)))
            return []

        user_comments = []

        for _comment in comments:
            user_comments.append(_comment['text'])
            del_res = self._vk_api.delete_comment(item_type, owner_id, object_id, _comment['id'])
            if del_res != 1:
                print("Cannot delete: {}".format(del_res))
        uns_res = self._vk_api.unsubscribe(item_type, owner_id, object_id)
        self._cache.add(uid)
        return user_comments

    def _load_cache(self):
        try:
            self._cache = set(
                [tuple(x) for x in json.loads(
                    open(self.CACHE_FILE).read()
                )]
            )
        except (FileNotFoundError, ValueError):
            pass

    def _save_cache(self):
        open(self.CACHE_FILE, mode="w").write(json.dumps(list(self._cache)))

    def _load_urls(self):
        try:
            self._urls = set(json.loads(open(self.URLS_FILE).read()))
        except (FileNotFoundError, ValueError):
            pass

    def _save_urls(self):
        open(self.URLS_FILE, mode="w").write(json.dumps(list(self._urls)))

