"""
%LENNY_FACE%
"""


class AccessTokenStorage:
    """
    OAuth token manipulating
    """

    NAME = '.access_token'

    @classmethod
    def load(cls):
        """ Load from FS """
        try:
            return open(cls.NAME, mode='r', encoding='utf-8').read()
        except FileNotFoundError:
            return None

    @classmethod
    def save(cls, at):
        """ Save to FS """
        open(cls.NAME, mode='w', encoding='utf-8').write(at)
