"""
%LENNY_FACE%
"""

import time

import vk

from .access_token import AccessTokenStorage

TIMEOUT = 0.5


def vk_safe_method(func):
    """ VK API Request decorator """
    def wrapper(*arg, **kwargs):
        """ Retry several times """
        for _ in range(5):
            try:
                time.sleep(TIMEOUT)
                return func(*arg, **kwargs)
            except vk.api.VkAPIError as vk_exc:
                s_exc = str(vk_exc)
                if 'Too many requests per second' in s_exc:
                    time.sleep(TIMEOUT)
                    continue
                print("VK API Error: " + s_exc)
                raise
            except Exception as exc:
                print("Common Exception: " + str(exc))
                raise
    return wrapper


class VKApiWrapper:
    """
    Wrapper for vk API
    """

    FROM_TIME = time.mktime(time.struct_time((1971, 0, 0, 0, 0, 0, 0, 0, 0)))
    TYPE_NAMESPACE_MAPPING = {
        'post': 'wall',
        'photo': 'photos',
        'topic': 'board',
        'video': 'video',
    }
    TYPE_OBJECT_MAPPING = {
        'post': 'post_id',
        'photo': 'photo_id',
        'topic': 'topic_id',
        'video': 'video_id',
    }
    TYPE_OWNER_MAPPING = {
        'post': 'owner_id',
        'photo': 'owner_id',
        'video': 'owner_id',
        'topic': 'group_id',
    }
    REDIRECT_URI = 'https://oauth.vk.com/blank.html'
    SCOPE = 274431
    VERSION = 5.85

    def __init__(self, vk_app_id):
        """ Initialize """
        self._client_id = vk_app_id
        self._at = AccessTokenStorage.load()
        self._api = None
        self._me = {}
        self._authorize()

    @property
    def user_id(self):
        """ return vk user id """
        return self._me[0]['id']

    @vk_safe_method
    def get_comments(self, item_type, owner_id, object_id, count=100):
        """ Get comments from post """
        comments = []
        params = {
            'count': count,
            'offset': 0,
            'owner_id': owner_id,
            self.TYPE_OBJECT_MAPPING[item_type]: object_id,
        }
        if item_type == 'topic':
            params['group_id'] = abs(owner_id)
        while True:
            time.sleep(TIMEOUT)
            response = getattr(
                self._api,
                self.TYPE_NAMESPACE_MAPPING[item_type]
            ).getComments(**params)
            comments_chunk = response['items']
            if not comments_chunk:
                return comments
            comments.extend([c for c in comments_chunk if c['from_id'] == self.user_id])
            params['offset'] += len(comments_chunk)

    @vk_safe_method
    def newsfeed_comments(self, count=100, start_time=FROM_TIME, **kwargs):
        """ Return comments from vk newsfeed """
        return self._api.newsfeed.getComments(
            count=count,
            start_time=start_time,
            **kwargs
        )

    @vk_safe_method
    def delete_comment(self, item_type, owner_id, object_id, comment_id):
        """
        Delete comment from vk

        Args:
            item_type (str): type of resource
            owner_id (int): owner id (person/group)
            object_id (int): post id
            comment_id (int): commend id

        Returns:
            dict: vk json result
        """
        return getattr(
            self._api,
            self.TYPE_NAMESPACE_MAPPING[item_type]
        ).deleteComment(**{
            self.TYPE_OWNER_MAPPING[item_type]:
                abs(owner_id) if item_type == 'topic' else owner_id,
            'topic_id': object_id,
            'comment_id': comment_id,
        })

    @vk_safe_method
    def unsubscribe(self, item_type, owner_id, iid):
        """ Unsubscribe from vk post """
        return self._api.newsfeed.unsubscribe(
            type=item_type,
            owner_id=owner_id,
            item_id=iid,
        )

    def _authorize(self):
        """ Authorize in vk.com """
        while True:
            try:
                session = vk.Session(access_token=self._at)
                self._api = vk.API(session, v=5.85)
                self._me = self._api.users.get()
                return
            except vk.api.VkAPIError as exc:
                for reason in (
                    "User authorization failed"
                ):
                    if reason in str(exc):
                        import webbrowser
                        webbrowser.open(self._auth_url)
                        print("Web browser should with access request page")
                        self._at = input("Please copy and enter your access token: ")
                        AccessTokenStorage.save(self._at)
                        break
                else:
                    raise

    @property
    def _auth_url(self):
        """ return oauth url """
        return (
            'https://oauth.vk.com/authorize?client_id={clid}&display=page&' +
            'redirect_uri={rurl}&scope={scope}&response_type=token&v={api_ver}&revoke=1'
        ).format(
            clid=self._client_id,
            rurl=self.REDIRECT_URI,
            scope=self.SCOPE,
            api_ver=self.VERSION
        )
