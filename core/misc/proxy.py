"""
%LENNY_FACE%
"""

import json
import requests
from random import choice

from bs4 import BeautifulSoup


class ProxyController:
    """
    Proxy manipulating class
    """

    NAME = '.proxies'
    URL = "https://free-proxy-list.net"
    LIMIT = 5

    def __init__(self):
        """ Load and download """
        self._proxies = self._load()
        self._proxy_idx = -1
        if not self._proxies:
            self._get_list_from_network()
        self._choose_idx()

    def __del__(self):
        """ Saving on deleting """
        self._save()

    @property
    def proxy(self):
        """ Return proxy """
        return self._proxies[self._proxy_idx][0]

    def swap(self):
        """ Choose new proxy """
        print("Proxy swapping")
        self._proxies[self._proxy_idx][1] += 1
        if self._proxies[self._proxy_idx][1] > self.LIMIT:
            del self._proxies[self._proxy_idx]
        self._choose_idx()

    def _choose_idx(self):
        """ Choose proxy randomly """
        self._proxy_idx = choice(list(self._proxies.keys()))

    def _get_list_from_network(self):
        """ Download proxy list from website """
        with requests.session() as _session:
            response = _session.get(self.URL)
            if not response.ok:
                return
            soup = BeautifulSoup(response.content, 'html.parser')
            table = soup.find("table", {"id": {"proxylisttable"}})
            for idx, tr in enumerate(table.find("tbody").find_all("tr")):
                ch = list(tr.children)
                self._proxies[idx] = [
                    "http://{}:{}".format(
                        ch[0].contents[0], ch[1].contents[0]
                    ), 0
                ]

    def _load(self):
        """ Load proxy list from file system """
        try:
            with open(self.NAME, mode='r') as fp:
                return json.load(fp, cls=list)
        except (FileNotFoundError, ValueError):
            return {}

    def _save(self):
        """ Save proxy list to file system """
        with open(self.NAME, mode='w') as fp:
            json.dump(self._proxies, fp)
