"""
%LENNY_FACE%
"""

import json
from random import choice


class UserAgentController:
    """
    Manipulating user agents
    """

    NAME = '.user_agents'
    DEFAULT = ('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.21'
               '(KHTML, like Gecko) Mwendo/1.1.5 Safari/537.21')
    LIMIT = 2

    def __init__(self):
        """ Load user agents from file system """
        try:
            self._agents = {
                int(k): [v[0], int(v[1])]
                for k, v in
                json.load(open(self.NAME)).items()
            }
        except (FileNotFoundError, ValueError):
            self._agents = []
        if not self._agents:
            self._agents = {0: [self.DEFAULT, -666]}
        for v in self._agents.values():
            v[0] = v[0][:-1]
        self._choose_idx()

    def __del__(self):
        """ Save on deleting """
        self._save()

    @property
    def agent(self):
        """ Return current user agent """
        return self._agents[self._current_agent_idx][0]

    def swap(self):
        """ Choose new user agent """
        print("User-agent swapping")
        self._agents[self._current_agent_idx][1] += 1
        if self._agents[self._current_agent_idx][1] > self.LIMIT:
            del self._agents[self._current_agent_idx]
        self._choose_idx()

    def _choose_idx(self):
        """ Choose user agent randomly """
        self._current_agent_idx = choice(list(self._agents.keys()))

    def _save(self):
        """ Save user agent list to filesystem """
        json.dump(self._agents, open(self.NAME, mode='w'))
