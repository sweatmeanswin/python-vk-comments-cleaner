"""
%LENNY_FACE%
"""

import os.path
import zipfile

import requests
from selenium import webdriver
from selenium.common.exceptions import WebDriverException


class ChromeDriver:
    """
    Selenium chrome driver manipualting
    """

    ARCHIVE_NAME = 'chromedriver_win32.zip'
    EXE_NAME = 'chromedriver.exe'
    VERSION_URL = 'https://chromedriver.storage.googleapis.com/LATEST_RELEASE'
    DOWNLOAD_URL_PATTERN = 'https://chromedriver.storage.googleapis.com/{version}/' + ARCHIVE_NAME
    CHROME_PATH = "C:\\ChromeDriver\\"
    DRIVER_PATH = os.path.join(CHROME_PATH, EXE_NAME)

    @classmethod
    def get(cls):
        """ Returns selenium chrome driver instance or download if not exists """
        try:
            return webdriver.Chrome(cls.DRIVER_PATH)
        except WebDriverException as exc:
            pass

        try:
            _session = requests.session()
            _response = _session.get(cls.VERSION_URL)
            _response.raise_for_status()
            version = _response.text
            _response = _session.get(cls.DOWNLOAD_URL_PATTERN.format(version=version))
            _response.raise_for_status()

            driver_dir = os.path.dirname(cls.CHROME_PATH)
            zip_path = os.path.join(driver_dir, cls.ARCHIVE_NAME)
            if not os.path.exists(driver_dir):
                os.makedirs(driver_dir)
            with open(zip_path, mode='wb') as zip_f:
                zip_f.write(_response.content)
            with zipfile.ZipFile(zip_path) as zip_f:
                zip_f.extract(cls.EXE_NAME, path=driver_dir)

            return webdriver.Chrome(cls.DRIVER_PATH)

        except requests.exceptions.RequestException as exc:
            raise Exception("Houston? {}".format(str(exc)))

        except (WebDriverException, Exception) as exc:
            raise Exception("Repair by yourself: {}".format(str(exc)))

