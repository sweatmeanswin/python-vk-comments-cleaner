"""
%LENNY_FACE%
"""

import re

from .base import BaseSearch, CaptchaError


class YaSearch(BaseSearch):
    """
    Yandex engine class
    """
    DOMAIN = 'yandex'
    GET_PARAM = 'text'
    LINK_PATTERN = re.compile('.*(https?.*)"}}')

    @classmethod
    def _update_params(cls, params):
        """ Update get request params """
        params['site'] = 'vk.com'
        if params['_page'] > 0:
            params['p'] = params['_page']

    def _parse_results(self, html_content):
        """
        Parsing html and looking for vk urls

        Args:
            html_content (str): html page content
        """
        if '<form class="form__inner" method="get" action="/checkcaptcha">' in html_content:
            raise CaptchaError("Yandex captcha")

        results = set()
        soup = self._get_soup(html_content)
        si = soup.findAll('li', {"class": {"serp-item"}})
        for serp_item in si:
            all_links = serp_item.findAll('div', {"class": {"organic"}})
            for a_tag in all_links:
                try:
                    res = self.LINK_PATTERN.search(a_tag['data-bem'])
                    url = res[1]
                except KeyError:
                    continue

                page_hash = hash(url)
                if page_hash in self._page_hashes:
                    continue
                self._page_hashes.add(page_hash)

                url = self._filter_result(url)
                if url and url not in results:
                    results.add(url)

        if not soup.find("a", {"class": {"pager__item_kind_next"}}):
            raise StopIteration(results)
        return list(results)
