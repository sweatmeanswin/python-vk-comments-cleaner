"""
%LENNY_FACE%
"""

from time import sleep
from urllib.parse import parse_qs, urlparse, urlencode

from bs4 import BeautifulSoup

from core.misc.chromedrive import ChromeDriver


class CaptchaError(Exception):
    """ Raise when engine return captcha """
    pass


class BaseSearch:
    """
    Base search engine class
    """

    DOMAIN = NotImplemented
    GET_PARAM = NotImplemented

    TIMEOUT = 1.0
    SCHEMA = 'https'

    def __init__(self):
        """ Create default resources """
        self._driver = ChromeDriver.get()
        self._page_hashes = set()

    def search(self, query, lang="ru", from_page=0, to_page=None):
        """
        Get SERP from search engine

        Args:
            query (str): search query (SURNAME)
            lang (str): "ru", "en"
            from_page (int): start search page
            to_page (int|None): stop search page, to the end if None

        Yields:
            str: url
        """
        current_page = from_page
        end_page = to_page

        self._open_home()
        while not end_page or current_page < end_page:
            print("{}#{}".format(self.DOMAIN, current_page + 1))
            page = self._get_page(query, lang, current_page)
            try:
                yield from self._parse_results(page)
            except CaptchaError as exc:
                print("Get {}. Please solve captcha and type OK".format(str(exc)))
                input()
                continue
            except StopIteration as exc:
                yield from exc.args[0]
                return

            current_page += 1

    def _open_home(self):
        """ Open search engine home page """
        self._driver.get("{}://{}.ru".format(self.SCHEMA, self.DOMAIN))

    def _get_page(self, query, lang, page):
        """ Open page """
        sleep(self.TIMEOUT)
        params = {
            self.GET_PARAM: query,
            'hl': lang,
            '_page': page,
        }
        self._update_params(params)
        del params['_page']
        self._driver.get('https://{}.ru/search?{}'.format(
            self.DOMAIN,
            urlencode(params),
        ))
        return self._driver.page_source

    @classmethod
    def _filter_result(cls, url):
        """ Filter url according rules """
        try:
            o = urlparse(url, cls.SCHEMA)
            if o.netloc and cls.DOMAIN not in o.netloc:
                return url

            if url.startswith('/url?'):
                url = parse_qs(o.query)['url'][0]
                o = urlparse(url, cls.SCHEMA)
                if o.netloc and cls.DOMAIN not in o.netloc:
                    return url
        except:
            return None
        return None

    @staticmethod
    def _get_soup(html_content):
        """ Return BeautifulSoup instance """
        return BeautifulSoup(html_content, 'html.parser')

    @classmethod
    def _update_params(cls, params):
        """ Update get request params in children """
        return NotImplemented

    def _parse_results(self, html_content):
        """ Process page to find all urls """
        raise NotImplementedError
