"""
%LENNY_FACE%
"""

from .base import BaseSearch, CaptchaError


class GoogleSearch(BaseSearch):
    """
    Google engine class
    """
    DOMAIN = 'google'
    GET_PARAM = 'q'
    PAGE_SIZE = 10
    TIMEOUT = 2.0

    @classmethod
    def _update_params(cls, params):
        """ Update get request params """
        params[cls.GET_PARAM] += ' site:vk.com'
        params['start'] = params['_page'] * cls.PAGE_SIZE

    def _parse_results(self, html_content):
        """
        Parse html

        Args:
            html_content (str): html content
        """
        if '<form id="captcha-form" action="index" method="post">' in html_content:
            raise CaptchaError("Google captcha")

        results = []
        soup = self._get_soup(html_content)
        for a_tag in soup.find(id='search').findAll('a'):
            if not a_tag.parent:
                continue

            try:
                url = a_tag['href']
            except KeyError:
                continue

            page_hash = hash(url)
            if page_hash in self._page_hashes:
                continue
            self._page_hashes.add(page_hash)

            url = self._filter_result(url)
            if url:
                results.append(url)

        nav_block = soup.find('div', {'id': {'navcnt'}})
        if not nav_block or not nav_block.find('a', {'id': {'pnnext'}}):
            raise StopIteration(results)
        return results
