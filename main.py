"""
%LENNY_FACE%
"""

from core import CommentsCleaner

VK_API_ID = -1

if __name__ == '__main__':
    with CommentsCleaner(
        vk_app_id=VK_API_ID,
        name='Name',
        surname='Surname',
    ) as cleaner:
        cleaner.remove_all_vk_comments()

    print("Finish")
